package sixthpackageforpractice;

// Java program to ReverseString using StringBuilder
import java.lang.*;


// Class of ReverseString
class ReverseString {
    public static void main(String[] args)
    {
        String data = "java is very easy";

        StringBuilder oolta = new StringBuilder();

        // append a string into StringBuilder oolta
        oolta.append(data);

        // reverse StringBuilder
        oolta.reverse();

        // print reversed String
        System.out.println(oolta);
    }
}
