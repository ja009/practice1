package sixthpackageforpractice;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ShuffleSongs {
    public static void main(String[] args) {
        List<String> songs = new ArrayList<String>();
        songs.add("sultans of swing");
        songs.add("brothers in arm");
        songs.add("tears in heaven");
        songs.add("limelight");
        songs.add("layla");
        shuffleList(songs);
        System.out.println(songs);
    }

    //public static void shuffleList(List<String> songsList) {
    public static void shuffleList(List<String> songs) {
        int n = songs.size(); //using the size of the array list and storing it in "n"
        Random random = new Random();//using the random class to randomize the values in the array list
        random.nextInt();//using nextInt we are going to the next value in
        for (int i = 0; i < n; i++) {
            int change = i + random.nextInt(n - i);
            swap(songs, i, change);
        }
    }

    public static void swap(List<String> songs, int i, int change) {
        String helper = songs.get(i);
        songs.set(i, songs.get(change));
        songs.set(change, helper);
    }





}

