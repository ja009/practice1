package firstpackageforpractice.methods;

import firstpackageforpractice.methods.Methods;

public class MethodsExe {
    public static void main(String[] args) {
        Methods object = new Methods();
        //object.testing(); This method cannot be called since it is private
        object.testing1();
        System.out.println("****");
        Methods.testing2();
        System.out.println("****");
        Methods.testing3();
        System.out.println("****");
        object.testing4();
        System.out.println("****");
        System.out.println("Calling a protected String value using object: "+ object.name);
        System.out.println("****");
        Methods.testing5("Soccer",1);//Calling a static method which has two parameters set to it

    }
}
