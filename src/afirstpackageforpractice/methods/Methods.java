package firstpackageforpractice.methods;

public class Methods {

    protected String name = "Jawad";
    private int age = 24;

    private void testing() {
        int a = 10;
        int b = 30;
        int c = a + b;
        System.out.println(c);
    }

    public void testing1() {
        int a = 7;
        int b = 1;
        int c = a + b;
        System.out.println(c);
    }

    public static void testing2() {
        int a = 3;
        int b = 1;
        int c = a + b;
        System.out.println(c);
    }

    protected static void testing3() {
        String a = "Hello";
        String b = "world";
        String c = a + b;
        System.out.println(c);

    }

    protected void testing4(){

        System.out.println("My name is "+name+" and my age is "+age);
    }

    static void testing5(String game, int num){
        System.out.println(game+" is "+num+" game in the world");
    }


}
