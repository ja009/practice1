package secondpackageforpractice;

public class AccessSpecifiers {
    public String rice1;//"Basmati"
    private String rice2;//"Brown"
    protected String veggie1;//"Brocoli"
    public String veggie2;//"Sweet Potato"
    static String protein;//"Salmon"
    public static char waterType; // "s or r"
    public int numOfSauce; // "3"


    public AccessSpecifiers(String rice1, String rice2, String veggie1, String veggie2, char waterType, int numOfSauce)  {//why doesn't the object work when these variables are in the ()
        this.rice1 = rice1;
        this.rice2 = rice2;
        this.veggie1 = veggie1;
        this.veggie2 = veggie2;
        this.waterType = waterType;
        this.numOfSauce = numOfSauce;
    }

    //1
    //Non Static
    public void choiceOfRice(){

        System.out.println("You are having "+ rice1 +" for your meal.");
    }
    //2
    //Non Static
    public void choiceOfVeggie(String veggie1){  //if you make this private, you won't be able to access it anywhere else
        System.out.println("You are having "+ veggie1 +" for your meal.");
    }
    //3
    //Static
    public static String choiceOfProtein(String protein){
        String meat = "Your choice of protein is " + protein;
        return meat;
    }
    //4
    //Static
    public static void choiceOfWater(char waterType){
        System.out.println(waterType +" is the type of water you have." );
    }
    //4
    //Non Static
    public String amountOfSauce(String numOfSauce){
        String sauce = "You have "+numOfSauce+" sauces.";
        return sauce;
    }

    public int amountOfSauce(){
        int sauce = 3;
        return sauce;
    }




}
