package secondpackageforpractice;

public class AccessSpecifiersExe {
    public static void main(String[] args) {


        AccessSpecifiers mealPrep = new AccessSpecifiers("basmati","brown","brocolli","beans",'x',3);

        //1
        mealPrep.choiceOfRice();

        //2
        mealPrep.choiceOfVeggie("Brocoli");

        //3
        String meat = AccessSpecifiers.choiceOfProtein("Salmon");
        System.out.println(meat);

        //4
        AccessSpecifiers.choiceOfWater('s'); // remember for char it is ' '

        //5
        String side = mealPrep.amountOfSauce("4");
        System.out.println(side);
    }
}
