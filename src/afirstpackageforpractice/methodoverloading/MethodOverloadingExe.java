package firstpackageforpractice.methodoverloading;

public class MethodOverloadingExe {
    public static void main(String[] args) {
        MethodOverloading object = new MethodOverloading();
        System.out.println("Performing a methodoverloading using different parameters:");
        MethodOverloading.testing("QA",130.4); //Since it is a static there won't be a need of the object.
        object.testing(12,01,1997);//Using obejct because the method is not a static method.
    }
}
