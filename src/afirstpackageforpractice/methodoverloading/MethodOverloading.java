package firstpackageforpractice.methodoverloading;

public class MethodOverloading {
    public static void testing(String job,double salary){
        System.out.println("I am a "+job+" tester "+" and my salary is "+salary+"k");
    }

    void testing(int month, int day,int year){
        System.out.println("My date of birth is "+month+"/"+day+"/"+year);
    }
}
