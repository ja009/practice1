package gseventhpackage;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class FileReaderUtils {

    public static void main(String[] args) throws IOException {
        String value = getPropertyOfFile("src/classwork/Config2.properties", "username");
        System.out.println(value);
    }

    public static String getPropertyOfFile(String filePath, String key) throws IOException {
        Properties properties = new Properties();

        // FileInputStream fs = new FileInputStream(filePath);
        // properties.load(fs);

        properties.load(new FileInputStream(filePath));

        // String value = properties.getProperty(key);
        // return value;

        return properties.getProperty(key);

    }
}
