package presentation;

public class ArrayPresentation {
    public static void main(String[] args) {

      String[] rappers = {"Drake","YE","Travis Scott","ASAP rocky"};
      System.out.println(rappers[0]);

      Object[] rappersInfo = new Object[4];
      rappersInfo[0]="Name";
      rappersInfo[1]="Age";
      rappersInfo[2]="Location";
      rappersInfo[3]="Genre";

      //10000ncodes
        rappersInfo[2]="Networth";

        for (int i = 0; i < rappers.length; i++){
            System.out.println(rappers[i]);
        }
        System.out.println("***********************");
        Object[][] rubric = new Object[3][3];

        //[Green]    [Blue]    [White]
        //[REd]    [Orange]    [Yellow]
        //[White]    [Green]    [Red]

        rubric[0][0]="Green";
        rubric[0][1]="Blue";
        rubric[0][2]="White";
        rubric[1][0]="Red";
        rubric[1][1]="Orange";
        rubric[1][2]="Yellow";
        rubric[2][0]="White";
        rubric[2][1]="Green";
        rubric[2][2]="Red";

        for (int row = 0; row < rubric.length; row++){
            for (int col = 0; col < rubric[row].length; col++){
                System.out.println(rubric[row][col]);
            }
        }

    }
}
