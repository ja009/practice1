package presentation;

import com.sun.security.jgss.GSSUtil;

import java.util.ArrayList;

public class ArrayListPresentation {
    public static void main(String[] args) {

        ArrayList<Object> rapper = new ArrayList<>();
        rapper.add("Drake");//0
        rapper.add(34);//1
        rapper.add(6.2);//2
        rapper.add(true);//3

        System.out.println(rapper);
        System.out.println(rapper.size());

        for(int i = 0; i < rapper.size(); i++){
            System.out.println(rapper.get(i));
        }
        System.out.println(rapper.get(1));
        rapper.remove(0);
        rapper.remove(6.2);
        System.out.println(rapper);

        System.out.println(rapper.isEmpty());
        rapper.clear();
        System.out.println(rapper.isEmpty());




    }
}
