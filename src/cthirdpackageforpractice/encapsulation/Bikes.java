package cthirdpackageforpractice;

public class Bikes {
    public String nameofbike;
    protected int year;
    static String model;

    public String getNameofbike() {
        return nameofbike;
    }

    public void setNameofbike(String nameofbike) {
        this.nameofbike = nameofbike;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public static String getModel() {
        return model;
    }

    public static void setModel(String model) {
        Bikes.model = model;
    }


    @Override
    public String toString() {
        return "Bikes{" +
                "nameofbike='" + nameofbike + '\'' +
                ", year=" + year +
                '}';
    }

}
