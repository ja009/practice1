package fifthpackageforpractice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class HashMapInArrayListTest {


    public static void main(String[] args){

        ArrayList<HashMap<Object,Object>> continents = new ArrayList<>();


        HashMap<Object, Object> northAmerica = new HashMap<>();

        northAmerica.put("USA","Washington D.C.");
        northAmerica.put("Canada","Toronto");
        northAmerica.put("Mexico","Mexico City");
        northAmerica.put("Cuba","IDK");

        continents.add(northAmerica);
        System.out.println(continents);

        System.out.println(continents.size());
        System.out.println(continents.get(0));
        continents.remove(0);
        System.out.println(continents);

        HashMap<Object, Object> cloning = (HashMap<Object, Object>) northAmerica.clone();
    }

}
