package fifthpackageforpractice;

import java.util.ArrayList;

public class ArrayListTest {


    public static void main(String[] args){

        ArrayList<String> trophies = new ArrayList<>();
        trophies.add("Champions"); //0
        trophies.add("Runners Up"); //1
        trophies.add("Man of the Match"); //2
        trophies.add("MVP"); //3
        trophies.add("Top Scorer"); //4

        System.out.println(trophies);
        System.out.println(trophies.size());

        System.out.println(trophies.get(1));

        trophies.remove(2);

        System.out.println(trophies);





    }

}
