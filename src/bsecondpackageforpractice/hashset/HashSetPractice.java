package fourthpackageforpractice;

import java.util.HashSet;
import java.util.Iterator;


//Hw for HashSet
public class HashSetPractice {
    public static void main(String[] args) {

        HashSet<String> setOfData = new HashSet<>();
        setOfData.add("NY");
        setOfData.add("NJ");
        setOfData.add("VA");
        setOfData.add("DC");
        setOfData.add("VA");

//1
//        Iterator<String> itr = setOfData.iterator();
//        while (itr.hasNext()) {
//            System.out.println(itr.hasNext());

//2
            for (String ele : setOfData) {
                System.out.println(ele + "");
            }

// 3
           // setOfData.forEach(System.out::println);
        }
    }

