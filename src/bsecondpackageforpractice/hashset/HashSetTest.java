package fifthpackageforpractice;

import java.util.HashSet;
import java.util.Iterator;

public class HashSetTest {
    public static void main(String[] args) {

        HashSet<String> nyc = new HashSet<String>();
        nyc.add("Queens");
        nyc.add("Brooklyn");
        nyc.add("Bronx");
        nyc.add("Manhattan");
        nyc.add("Long Island");

        System.out.println(nyc);

        nyc.remove("Bronx");
        System.out.println(nyc);

        Iterator<String> itr = nyc.iterator();
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }

    }
}
