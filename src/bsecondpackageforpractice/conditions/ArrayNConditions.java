package fourthpackageforpractice;

public class ArrayNConditions {
    public static void main(String[] args) {

        String [] UsStates = {"NY","NJ","CT","MI","PA"};

        for (int i = 0; i<UsStates.length; i++){
            if(UsStates[i].equals("NY")||UsStates[i].equals("NJ")||UsStates[i].equals("CT")){
                System.out.println(UsStates[0]+", "+UsStates[1]+" and "+UsStates[2]+" are called the Tristate.");
            }else{
                System.out.println(UsStates[3]+" and "+UsStates[4]+" are normal states.");
            }
        }

        System.out.println("**********************************");

        for (int i = 0; i < UsStates.length; i++){
            if(i==0||i==1||i==2){
                System.out.println("The Tristate.");
            }else {
                System.out.println("Normal states.");
            }
        }
    }
}
