package fourthpackageforpractice;

public class ArrayNConditions2 {
    public static void main(String[] args) {

        String [][] personalInfo = new String [5][5]; //For String Arrays you can put the values in here, based on that the size is set and  set the size/slots of the array
        //Object [][] sensetiveInfo = {{"Jawad",12},{"Md",5}};//For Object arrays you can put the values in, based on that the size is set and  set the size/slots of the array
        personalInfo[0][0]="Jawad";
        personalInfo[0][1]="23";
        personalInfo[0][2]="NY";
        personalInfo[0][3]="Student";
        personalInfo[0][4]="Single";
        System.out.println(personalInfo.length);
        System.out.println(personalInfo[0][0]);
        System.out.println(personalInfo[0][1]);
        System.out.println(personalInfo[0][2]);
        System.out.println(personalInfo[0][3]);
        System.out.println(personalInfo[0][4]);

        //personalInfo[][]



//                [0,0] [0,1] [] [] []
//                [1,0] [1,1]
//                [2,0] [2,1]
//                [3,0] [3,1]
//                [4,0] [4,1]

    }
}
