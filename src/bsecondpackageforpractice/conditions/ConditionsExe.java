package thirdpackageforpractice;


public class ConditionsExe {
    public static void main(String[] args) {
        Conditions mealPrep = new Conditions();

        mealPrep.choiceOfRice("Basmati");
        mealPrep.choiceOfRice("Brown");
        mealPrep.choiceOfRice("idk");

        Conditions.choiceOfVeggie("Brocolli");
        Conditions.choiceOfVeggie("Sweet Potato");
        Conditions.choiceOfVeggie("idk");
    }
}
