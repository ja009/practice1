package fifthpackageforpractice;

import java.util.ArrayList;
import java.util.HashMap;

public class ArrayListInHashMapTest {

    public static void main(String[] args){

        HashMap<Object, ArrayList<Object>> sports = new HashMap<>();
        ArrayList<Object> soccer = new ArrayList<>();
        soccer.add("Real Madrid");
        soccer.add("Liverpool");
        soccer.add("Man United");
        soccer.add("Man City");



        sports.put("Num 1:", soccer);
        System.out.println(sports);
        System.out.println(sports.size());
        System.out.println(sports.get("Num 1:"));
        System.out.println(sports.get("Num 1:").get(0));
        sports.remove("Num 1:",soccer.remove(0));//This is how you remove the values in Array list using the HashMap
        System.out.println(sports.get("Num 1:"));



    }
}
