package fourthpackageforpractice;

import java.util.HashMap;

public class HashMapPractice {
    public static void main(String[] args) {

        HashMap<String,Integer> nameNAge = new HashMap<>();

        nameNAge.put("Cristiano",36);
        nameNAge.put("Nadal",37);
        nameNAge.put("Jordan",52);

        System.out.println(nameNAge);
        System.out.println(nameNAge.get(2));
        nameNAge.remove("Nadal");
        System.out.println(nameNAge);




    }
}
