package fifthpackageforpractice;

import java.util.HashMap;

public class HashMapTest {
    public static void main(String[] args){

        HashMap<Object,Object> juices = new HashMap<>();
        juices.put(1,"Orange");
        juices.put(2,"Mango");
        juices.put(3,"Beet");
        juices.put(4,"Cranberry");


        System.out.println(juices);
        System.out.println(juices.size());
        System.out.println(juices.get(1));
        juices.remove(2);
        System.out.println(juices);


    }
}
